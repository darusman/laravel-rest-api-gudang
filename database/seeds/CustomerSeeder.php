<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        foreach(range(0,100) as $i){
            DB::table('customers')->insert([
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'telp' => $faker->phoneNumber
                // 'date_of_birth' => $faker->dateTimeThisCentury()->format('Y-m-d')
            ]);
        }
    }
    
}
