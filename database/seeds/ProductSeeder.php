<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        
        $faker = Faker::create();
        foreach(range(0,100) as $i){
            DB::table('products')->insert([
                'nama' => $faker->company,
                'stok' => $faker->unique()->numberBetween($min = 50, $max = 200),
                'harga' => $faker->unique()->numberBetween($min = 1000, $max = 10000),
                'kadaluarsa' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'status' => $faker->randomElement(['baik','rusak'])
            ]);
        }
    }
}
