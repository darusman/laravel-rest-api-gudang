<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Customer;
use Illuminate\Support\Facades\Response;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customerList = Customer::paginate(10);
        return response()->json($customerList, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $rules = [
        //     'nama' => 'required|min:2',
        //     'alamat' =>'required|min:3',
        //     'image' =>  'required|image',
            
        // ];

        // $validator = Validator::make($request->all(), $rules);
        // if($validator->fails()){
        //     return response()->json($validator->errors(), 400);
        // }

        // $customer = Customer::find($id);
        // if(is_null($customer)) {
        //     return response()->json(["message" => "Record Not Found"], 404);
        // }
        // $customer = Customer::create($request->all());
        // $imageName = time().'.'.$request->image->extension();  
        // $request->image->move(public_path('images'), $imageName);
        // return response()->json($customer, 201);

        $request->validate([
            'nama'    =>  'required',
            'alamat'  =>  'required',
            'telp'    =>  'required',
        ]);

        // cara 1
        // $image = $request->file('image');
        // $fileName = "user_image.jpg";
        // $image->move(public_path('images'), $fileName);
        // $photoURL = url('/'.$fileName);
        // $form_data = array(
        //     'nama'       =>   $request->nama,
        //     'image'            =>   $fileName,
        //     'alamat'        =>   $request->alamat,
        //     'telp'            =>   $request->telp,
        // );

        // cara 2
    //     $image_file = $request->user_image;
    //     $image = Image::make($image_file);   
    //     Response::make($image->encode('jpeg'));

    //     $form_data = array(
    //     'nama'       =>   $request->nama,
    //     'image'            =>   $fileName,
    //     'alamat'        =>   $request->alamat,
    //     'telp'            =>   $request->telp,
    //    );
   
        $customer = Customer::create($request->all());
        return response()->json($customer, 200);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customer = Customer::find($id);
        if(is_null($customer)){
            return response()->json(["message" => "Record Not Found"]);
        }
        return response()->json($customer, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $customer = Customer::find($id);
        if(is_null($customer)) {
            return response()->json(["message" => "Record Not Found"], 404);
        }
        $customer->update($request->all());
        return response()->json($customer, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $customer = Customer::find($id);
        if(is_null($customer)) {
            return response()->json(["message" => "Record Not Found"], 404);
        }
        $customer->delete();
        return response()->json(null, 204);
    }
}
